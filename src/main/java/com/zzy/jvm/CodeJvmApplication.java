package com.zzy.jvm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeJvmApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeJvmApplication.class, args);
    }


}
