package com.zzy.jvm.c3_memory_alloc;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;


/**
 * 栈上分配
 * -Xmx15m -Xms15m -XX:+PrintGC -XX:+DoEscapeAnalysis -XX:+EliminateAllocations
 * 使用如下参数都会发生大量GC
 * -Xmx15m -Xms15m -XX:+PrintGC -XX:-DoEscapeAnalysis -XX:+EliminateAllocations
 * -Xmx15m -Xms15m -XX:+PrintGC -XX:+DoEscapeAnalysis -XX:-EliminateAllocations
 */
@Slf4j
public class StackAllot {

    @Data
    static class Good{
        private Long id;
        private String desc;
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000000; i++) {
            alloc();
        }
        long end = System.currentTimeMillis();
        log.info("程序执行结束，总共用时：{}ms",end-start);
    }

    private static void alloc(){
        Good good = new Good();
        good.setId(1L);
        good.setDesc("三花淡奶");
        //保存商品信息
    }

}
