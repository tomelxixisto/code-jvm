package com.zzy.jvm.c7_jvm;

public class DeadLock {

    private static Object lockA = new Object();
    private static Object lockB = new Object();

    public static void main(String[] args) {
        
        new Thread(() ->{
            synchronized (lockA) {
                try {
                    System.out.println("线程1开始运行========");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
                synchronized (lockB) {
                    System.out.println("线程1运行结束========");
                }
            }
        }).start();

        new Thread(() ->{
            synchronized (lockB) {
                try {
                    System.out.println("线程2开始运行========");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                }
                synchronized (lockA) {
                    System.out.println("线程2结束运行========");
                }
            }
        }).start();
        
        System.out.println("主线程运行结束========");
    }
}